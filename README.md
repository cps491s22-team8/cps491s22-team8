University of Dayton

Department of Computer Science

CPS 491 - Spring 2021

Instructor: Dr. Phu Phung

### Repositories 

Public Repo: https://bitbucket.org/cps491s22-team8/cps491s22-team8.bitbucket.io/src/master/

Private Repo: https://bitbucket.org/cps491s22-team8/cps491s22-team8/src/main/

## Capstone II Proposal 


# Project Topic: Train an NLP Model Using Decentralized Computing Code

# Team members

1. Wesam Haddad, haddadw1@udayton.edu
2. Quan Nguyen, nguyenq2@udayton.edu
3. Shara Shrestha, shresthas6@udayton.edu
4. Cole Weslow, weslowc1@udayton.edu


# Company Mentors

1. Kathryn Rebecca Servaites, _Staff Computer Engineer_
2. Colin Leong; _Staff ML Researcher_
3. Ian Cannon; _AF Researcher_

University of Dayton Research Institute
1700 S Patterson Blvd, Dayton, OH 45469

## Project Management Information

Team Homepage: [https://cps491s22-team8.bitbucket.io](https://cps491s22-team8.bitbucket.io)

Project Board: https://trello.com/b/l6pdLLEv/dedloc-project

# Overview

![Overview Architecture](https://media.discordapp.net/attachments/892876404084064256/921289383867465768/centralised-decentralised-1024x474.png "A Sample of Overview Architecture")

Figure 1. - Centralized Versus Decentralized Computing for Machine Learning

# Project Context and Scope

Our project is to design a tool that will allow training machine learning models to be streamlined. 
Using the DeDLOC library we will be able to allow users to donate some of their computing power so that UDRI can train models quickly without relying on a single computer.
Once we have trained our own natural language processing model using DeDLOC, we will create a GUI that will let UDRI employees to train models. Once these models are made, 
admins will be able to monitor them and analyze their accuracy and overall effectiveness. We also will be conducting research on whether or not the DeDLOC library poses any security threats, and if
UDRI could actually implement this tool in house. 

# High-level Requirements

* Research and evaluate DeDLOC for usability and security
* Use DeDLOC to train a machine learning model
* Build a tool with DeDLOC integration
* Document every process


## Use cases

### Overview diagram
![Use Case Diagram](https://cdn.discordapp.com/attachments/892876404084064256/932848376729468959/TP4_R_8m4CPtVufJfFVT6FW3OqMWGuTsAQMT3-U04uulii-bgEftBqP9r5Hgff-iv_SuTrv6nQ2fTYQUs3SOi0QqAc6Z5qVm8Cx96OoZlO8A13uS5IeEP9N5criKYcsaK016ABkJighPtuvMnZHeJtWWAEuvQk1TKgfWBNMJbFq1NaHS0Py6hb-AP37cg7XKWdyWUx2tA0LevDn.png)     


#### Donate Percentage of Computer's Power    
 * Actor: User and Admin
 * User Story: Users can donate CPU power to training models
 * Use Case Description: Any user will be able to allocate a percentage of their CPU power to help train a machine learning model over a distributed system

#### Setup Experiment as Coordinator
 * Actor: User and Admin
 * User Story: Users can setup experiments as coordinators
 * Use Case Description: Any user will be able to create their own machine learning model to train and act as the central computer in the experiment.

#### Link to Other Public Experiments
 * Actor: User and Admins
 * User Story: Users and Admins can connect to other experiments
 * Use Case Description: Any User will be able to search through a catalogue of open experiments and choose which ones to connect with

#### View Results and Analytics of Training Model
 * Actor: Users and Admins
 * User Story: Users and Admins can view their results and analytics of their trained models
 * Use Case Description: After training is complete they will be able to check the accuracy and stats of their trained model and use the new model

#### Manage All Public Experiments
 * Actor: Admin
 * User Story: Admins can access and manage all public experiments 
 * Use Case Description: Any user that creates an experiment, an admin can access it and look at it's information

#### Monitor Models via Weights & Biases
 * Actor: Admin
 * User Story: Admins can monitor all training models using tools like Weights & Biases
 * Use Case Description: Admins will be able to monitor all training models so that they can see the progress of everything
 
# System Design
![Training Process](https://media.discordapp.net/attachments/882356965022515274/969430216411848724/epoch_1.png)
Figure 2. - This is a simple diagram explaining the training process of the model, using hivemind and how they average the loss between peers. 
Also, each peer is given different batches of the training data.

![Workflow](https://media.discordapp.net/attachments/882356965022515274/969436526851284992/Blank_diagram.png)
Figure 3. - This is an overview of our tool using hivemind and the general workflow required of initial users, and subsequent users.

## Database
For the dataset(s), we will be using Hugging Face for storage, and Weights and Biases for data charts/graphs.

## User Interface

![Tool Interface](https://media.discordapp.net/attachments/882356965022515274/958394524571414608/unknown.png)
![Running Instance](https://media.discordapp.net/attachments/882356965022515274/958395238106419210/unknown.png)

# Implementation 

## Deployment

Our plans for future deployment will be to create an Ubuntu 18.04 docker image instead of using WSL for training these models

# Technology

For this project we will be using the DeDLOC library, ALBERT, and OSCAR library. We will be programming this project using Python.

Windows Subsystem for Linux (WSL) to run hivemind in Windows. We are using Python 3.8.2, hivemind 0.9.9 and its dependencies.

TKinter will be used to create a tool to assist in running hivemind.

# Impacts

Normally when creating/training an AI you use high performance computers that continuously run until the training is done.
But creating an NLP model using decentralized code will make it easier to train AIs for machine learning, as anyone with a computer could assist in training. 
Those assisting in training can disconnect or connect whenever they would like, and they wouldn't even need high performing equipment/parts to assist.


# Software Process Management

![Spring 2022 Timeline on Trello](https://cdn.discordapp.com/attachments/892876404084064256/921292445256405042/unknown.png "Spring 2022 Timeline")

## Scrum process

### Sprint 0

Duration: 01/01/2022-01/18/2022

#### Tasks: 

1. Create Sprint 0 branch on BitBucket
2. Create Slack communication channel
3. Tools/software installation
4. Set up environment
5. Flush out backlog for user requirements

### Completed Tasks:

1. Create Sprint 0 branch on BitBucket
2. Create Slack communication channel

#### Contributions: 

1.  Wesam Haddad, 4 commits, 10 hours, contributed in Set up environment, Bitbucket, Tools/software
2.  Quan Nguyen, 12 commits, 10 hours, contributed in Set up environment, Bitbucket, Tools/software
3.  Shara Shrestha, 2 commits, 7 hours, contributed in Team Homepage Design, and Trello
4.  Cole Weslow, 5 commits, 8 hours, contributed in Updating of README.md, Trello, and Slack

#### Sprint Retrospection:

| Good     |   Could have been better    |  How to improve?  |
|----------|:---------------------------:|------------------:|
|          |   Completing More Tasks     | Work more efficiently?|

### Sprint 1

Duration: 01/18/2022-02/25/2022

#### Tasks: 

1. Tools/software installation
2. Set up environment
3. Flush out backlog for user requirements
4. Conduct general research on natural language processing
5. Conduct general research on distributed computing
6. Learn python
7. Update Team Homepage
8. Update Sprint README.md

### Completed Tasks:
 
 1. Tools/software installation
 2. Set up environment
 3. Flush out backlog for user requirements
 4. Partial completion of Update Team Homepage
 5. Update Sprint README.md

#### Contributions: 

1.  Wesam Haddad, 12 hours, contributed in Tools/software, Set up environment, General Research, Python
2.  Quan Nguyen, 13 hours, contributed in Tools/software, Set up environment, General Research, Python, Backlog
3.  Shara Shrestha, 1 commits, 8 hours, contributed in Team Homepage, Backlog
4.  Cole Weslow, 3 commits, 7 hours, contributed in README.md, General Research, Set up environment, Backlog, Tools/software

#### Sprint Retrospection:

| Good     |   Could have been better    |  How to improve?  |
|----------|:---------------------------:|------------------:|
|   Getting Tasks Completed       |                             |                   |
|   Communication Between Team Members       |        Better communication with clients                     |         Use the Slack more to ask questions        |


### Sprint 2

Duration: 01/25/2022-02/01/2022

#### Tasks: 

1. Research DeDLOC library
2. Gather datasets with good data
3. Conduct general research on natural language processing
4. Conduct general research on distributed computing
5. Learn Python
6. Continue Updating Team Homepage
7. Update Sprint README.md
8. Replicate sahajBERT training model using DeDLOC

### Completed Tasks:

1. Continue Updating Team Homepage
2. Update Sprint README.md
3. Better Understanding of sahajBERT

#### Contributions: 

1.  Wesam Haddad, 10 hours, contributed in Project Research, Python, sahajBERT
2.  Quan Nguyen, 11 hours, contributed in Project Research, Python, sahajBERT
3.  Shara Shrestha, 1 commits, 7 hours, contributed in Team Homepage
4.  Cole Weslow, 3 commits, 7 hours, contributed in Update README.md, Trello

#### Sprint Retrospection: 

| Good     |   Could have been better    |  How to improve?  |
|----------|:---------------------------:|------------------:|
|   Communication       |   Asking for help from UDRI                          |  We set up meeting with UDRI for help and feedback    |
|   Research        | Dedicate more time to project | |

### Sprint 3

Duration: 02/01/2022-02/8/2022

#### Tasks: 

1. Research DeDLOC library
2. Gather datasets with good data
3. Conduct general research on natural language processing
4. Conduct general research on distributed computing
5. Learn Python
6. Continue Updating Team Homepage
7. Update Sprint README.md
8. Replicate sahajBERT training model using DeDLOC
9. Presentation 1

### Completed Tasks:
1. Research DeDLOC library
2. Conduct general research on natural language processing
3. Conduct general research on distributed computing
4. Learn Python
5. Continue Updating Team Homepage
6. Update Sprint README.md
7. Create Wiki Page for documentation
8. Presentation 1

#### Contributions: 

1.  Wesam Haddad, 3 commits, 10 hours, contributed in Research DeDLOC library, NLP, and distributed computing, Update Sprint README.md, Learn Python
2.  Quan Nguyen, 2 commits, 15 hours, contributed in Create Wiki Page for documentation, Learn Python
3.  Shara Shrestha, 3 commits, 10 hours, contributed in Continue Updating Team Homepage, Learn Python, Presentation 1
4.  Cole Weslow, 5 commits, 12 hours, contributed in Update Sprint README.md, Learn Python

#### Sprint Retrospection: 

| Good     |   Could have been better    |  How to improve?  |
|----------|:---------------------------:|------------------:|
| Documentation    |  Finding time to work                         |        Work on time management           |

### Sprint 4

Duration: 02/08/2022-02/17/2022

#### Tasks: 

1. Research DeDLOC library
2. Gather datasets with good data
3. Continue - Conduct general research on natural language processing
4. Continue - Conduct general research on distributed computing
5. Continue - Learn Python
6. Continue - Updating Team Homepage
7. Continue - Update Sprint README.md
8. Create GUI layout/function outline
9. Set-up and install Tkinter for GUI

### Completed Tasks:

1. Continue - Conduct general research on natural language processing
2. Continue - Conduct general research on distributed computing
3. Continue - Learn Python
4. Continue - Updating Team Homepage
5. Continue - Update Sprint README.md
6. Partial Completion of GUI layout/function outline
7. Set-up and install Tkinter for GUI

#### Contributions: 

1.  Wesam Haddad, 15 hours, contributed in Reasearch DeDLOC library, Conduct general research, Learn Python, Create/Set-up GUI layout/function outline
2.  Quan Nguyen, 14 hours, contributed in Research DeDLOC library, Conduct general research, Learn Python, Create/Set-up GUI layout/function outline
3.  Shara Shrestha, 11 hours, contributed in Conduct general research, Updating Team Homepage, Create/Set-up GUI layout/function outline
4.  Cole Weslow, 11 hours, contributed in Research DeDLOC library, Conduct general research, Learn Python, Update README.md

#### Sprint Retrospection: 

| Good     |   Could have been better    |  How to improve?  |
|----------|:---------------------------:|------------------:|
|    Documentation      |       Allocating more time for the project           |        Work on time management           |

### Sprint 5

Duration: 02/17/2022-03/01/2022

#### Tasks: 

1. Continue - Conduct general research on natural language processing
2. Continue - Conduct general research on distributed computing
3. Continue - Updating Team Homepage
4. Continue - Update Sprint README.md
5. Continue - Research DeDLOC library
6. Continue - Gather datasets with good data
7. Continue - Create GUI layout/function outline
8. Presentation

### Completed Tasks:

1. Conduct general research on natural language processing
2. Conduct general research on distributed computing
3. Create GUI layout/function outline
4. Presentation

#### Contributions: 

1.  Wesam Haddad, 14 hours, contributed in Conduct general research, Research DeDLOC library, Create GUI layout/function outline, Presentation
2.  Quan Nguyen, 14 hours, contributed in Conduct general research, Research DeDLOC library, Create GUI layout/function outline, Presentation
3.  Shara Shrestha, 11 hours, contributed in Conduct general research, Update Team Homepage, Create GUI layout/function outline, Presentation
4.  Cole Weslow, 12 hours, contributed in Conduct general research, Update README.md, Research DeDLOC library, Presentation

#### Sprint Retrospection: 

| Good     |   Could have been better    |  How to improve?  |
|----------|:---------------------------:|------------------:|
|   Communication       |     Balancing Classwork/ Allocate more time          |                   |
|          |    Learning another group's codebase   | Give more time to learning the codebase   |

### Sprint 6

Duration: 03/01/2022-03/08/2022

#### Tasks: 

1. Replicate Albert training model using DeDLOC
2. Continuous - Create Documentation on DeDLOC
3. Continuous - Updating Team Homepage
4. Continuous - Update README.md
5. Gather datasets with good data
6. Further develop tool

### Completed Tasks:
1. Continuous - Create Documentation on DeDLOC/Hivemind
2. Continuous - Updating Team Homepage
3. Continuous - Updating README.md
4. Partially Complete - Replicate Albert training model using DeDLOC
5. Further develop tool

#### Contributions: 

1.  Wesam Haddad, contributed in Create Documentation, Replicate Albert
2.  Quan Nguyen, contributed in Create Documentation, Replicate Albert, Further develop tool
3.  Shara Shrestha, contributed in Updating Team Homepage, Further develop tool
4.  Cole Weslow, contributed in Updating README.md, Replicate Albert

#### Sprint Retrospection: 

| Good     |   Could have been better    |  How to improve?  |
|----------|:---------------------------:|------------------:|
|     Communication     |            More time allocation                 |         Work on time management          |

### Sprint 7

Duration: 03/08/2022-03/10/2022 

#### Tasks: 

1. Upload training model to Hugging Face
2. Replicate Albert training model using DeDLOC
3. Continuous - Create Documentation on DeDLOC
4. Continuous - Updating Team Homepage
5. Continuous - Update README.md
6. Gather datasets with good data
7. Further develop tool

### Completed Tasks:

1. Partially Complete - Replicate Albert training model using DeDLOC
2. Continuous - Create Documentation on DeDLOC
3. Continuous - Updating Team Homepage
4. Continuous - Update README.md
5. Further develop tool

#### Contributions: 

1.  Wesam Haddad, contributed in Replicate Albert, Create Documentation
2.  Quan Nguyen, contributed in Create Documentation, Further develop tool
3.  Shara Shrestha, contributed in Updating Team Homepage, Further develop tool
4.  Cole Weslow, contributed in Update README.md, Replicate Albert

#### Sprint Retrospection: 

| Good     |   Could have been better    |  How to improve?  |
|----------|:---------------------------:|------------------:|
|    Completed most of the tasks for sprint     |            Complete tasks in a more timely manner                 |         Work on task management          |
|    Communication     |                             |                   |

### Sprint 8

Duration: 03/10/2022-03/22/2022

#### Tasks: 

1. Upload training model to Hugging Face
2. Replicate Albert training model using DeDLOC
3. Continuous - Create Documentation on DeDLOC
4. Continuous - Updating Team Homepage
5. Continuous - Update README.md
6. Gather datasets with good data
7. Further develop tool

### Completed Tasks:

1. Partially Complete - Upload training model to Hugging Face
2. Partially Complete - Replicate Albert training model using DeDLOC
3. Continuous - Create Documentation on DeDLOC
4. Continuous - Updating Team Homepage
5. Continuous - Update README.md
6. Further develop tool

#### Contributions: 

1.  Wesam Haddad, contributed in Upload to Hugging Face, Replicate Albert, Create Documentation
2.  Quan Nguyen, contributed in Replicate Alber, Create Documentation, Furhter develop tool
3.  Shara Shrestha, contributed in Updating Team Homepage, Further develop tool
4.  Cole Weslow, contributed in Replicate Albert, Hugging Face, Update README.md

#### Sprint Retrospection: 

| Good     |   Could have been better    |  How to improve?  |
|----------|:---------------------------:|------------------:|
|    Team Communication      |              Task delegation               |                   |
|    Completion of tasks      |                             |                   |

### Sprint 9

Duration: 03/22/2022-03/29/2022

#### Tasks: 

1. Upload training model to Hugging Face
2. Replicate Albert training model using DeDLOC
3. Continuous - Create Documentation on DeDLOC
4. Continuous - Updating Team Homepage
5. Continuous - Update README.md
6. Gather datasets with good data
7. Further develop tool
8. Interpret results of training model
9. Presentation 3

### Completed Tasks:

1. Partially Complete - Upload training model to Hugging Face
2. Partially Complete - Replicate Albert training model using DeDLOC
3. Continuous - Create Documentation on DeDLOC
4. Continuous - Updating Team Homepage
5. Continuous - Update README.md
6. Further develop tool
7. Partially Complete - Interpret results of training model
8. Presentation 3

#### Contributions: 

1.  Wesam Haddad, contributed in Upload to Hugging Face, Replicate Albert, Create Documentation, Interpret results, Presentation 3
2.  Quan Nguyen, contributed in Replicate Albert, Create Documentation, Further develop tool, Presentation 3
3.  Shara Shrestha, contributed in Updating Team Homepage, Further develop tool, Presentation 3
4.  Cole Weslow, contributed in Replicate Albert, Update README.md, Presentation 3

#### Sprint Retrospection: 

| Good     |   Could have been better    |  How to improve?  |
|----------|:---------------------------:|------------------:|
|    Work ethic      |             Task Delegation                |                   |
|    Team Communication      |                             |                   |

### Sprint 10

Duration: 03/29/2022-04/05/2022

#### Tasks: 

1. Analyze DeDLOC's usability
2. Analyze DeDLOC's security issues
3. Research user configurations
4. Create tool with DeDLOC integration
5. Create documentation for tool
6. README.md updates
7. Team homepage updates

### Completed Tasks:

1. Analyze DeDLOC's usability
2. Research user configurations
3. Continue - Create tool with DeDLOC/Hivemind integration
4. Continue - Create documentation for tool
5. Team homepage updates

#### Contributions: 

1.  Wesam Haddad, contributed in Analyze DeDLOC's usability, Research user configurations, Create documentation
2.  Quan Nguyen, contributed in Analyze DeDLOC's usability, Research user configurations, Create documentation, Create tool with integration
3.  Shara Shrestha, contributed in Analyze DeDLOC's usability, Team homepage updates
4.  Cole Weslow, x commits, y hours, contributed in Analyze DeDLOC's usability, Research user configurations

#### Sprint Retrospection: 

| Good     |   Could have been better    |  How to improve?  |
|----------|:---------------------------:|------------------:|
|    Team Communication      |                             |                   |

### Sprint 11

Duration: 04/05/2022-04/12/2022

#### Tasks: 

1. Continue Tool Development
2. Continue Documentation/Wiki

### Completed Tasks:

1. Continue Tool Development
2. Continue Documentation/Wiki 

#### Contributions: 

1.  Wesam Haddad, contributed in Documentation/Wiki, Tool Development
2.  Quan Nguyen, contributed in Documentation/Wiki, Tool Development
3.  Shara Shrestha, contributed in Tool Development
4.  Cole Weslow, contributed in Tool Development

#### Sprint Retrospection: 

| Good     |   Could have been better    |  How to improve?  |
|----------|:---------------------------:|------------------:|
|    Task Completion      |            Communication                 |         Communicate more effectively          |

### Sprint 12

Duration: 04/12/2022-04/19/2022

#### Tasks: 

1. Finalize Tool
2. Finalize Documentation
3. Final Presentation

### Completed Tasks:

1. Finalize Tool
2. Finalize Documentation
3. Final Presentation

#### Contributions: 

1.  Wesam Haddad, contributed in Finalize Tool, Finalize Documentation, Final Presentation
2.  Quan Nguyen, contributed in Finalize Tool, Finalize Documentation, Final Presentation
3.  Shara Shrestha, contributed in Finalize Tool, Final Presentation
4.  Cole Weslow, contributed in Final Presentation

#### Sprint Retrospection: 

| Good     |   Could have been better    |  How to improve?  |
|----------|:---------------------------:|------------------:|
|    Team Communication      |             Work Efficiency                |                   |


# User guide/Demo

Write as a demo with screenshots and as a guide for users to use your system.\
Our project [Demo](https://youtu.be/cwLx4p5KiEI) can be found here.


# Company Support

After discussing with the company they offered support with any questions we have related to the project, and assistance acquiring more user machines to test the machine learning. Our communication plan is to contact them through email or slack, and hold semi-weekly update meetings if necessary.

# Acknowledgments 

Thank you so much to Kathryn Rebecca Servaites, Colin Leong, and Ian Cannon from UDRI for being our mentors during this project.