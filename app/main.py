import subprocess as sub
import asyncio
from threading import Thread

from torch_service import TorchService
from hivemind_service import HivemindService

import tkinter as tk
from tkinter import ttk

# Service initialization.
torch: TorchService = TorchService()
hivemind: HivemindService = HivemindService()
torch.start()
hivemind.start()

# Customization
fg = 'white'
bg = '#353535'
font = ("Arial", 18)

# Tkinter initialization
root = tk.Tk()

# Tkinter settings
root.title('Alberto')
root.geometry('500x400')
root.configure(background=bg)
root.grid()


def start(_pb: ttk.Progressbar):
    t = Thread(target=trainer, daemon=True)
    t.start()
    _pb.start()


def trainer():
    global torch, hivemind
    hivemind.init_dht()
    hivemind.print_dht_address()
    hivemind.init_opt(opt=torch.opt, run_id="my_cifar_run", verbose=True)
    torch.start_training()
    torch.train_data(hivemind.opt)


def stop(_pb: ttk.Progressbar):
    global torch
    torch.stop_training()
    _pb.stop()


# Text
header = ttk.Label(root, text='Alberto DeDLOC Tool', foreground=fg,
                   background=bg, font=font, justify=tk.CENTER, borderwidth=5)
header.place(relx=0.5, rely=0.1, anchor='center')
# tk.Label(root, text='Albert DeDLOC Tool', fg='White', bg='#353535').pack()

# Buttons
# startBtn = tk.Button(root, text='Start', width=10, command=start)
# startBtn.pack(expand=tk.FALSE, fill=tk.X, side=tk.TOP)

# tk.Button(root, text='Exit', width=10, command=root.destroy).pack()
# startBtn.pack(expand=tk.FALSE, fill=tk.X, side=tk.TOP)

# Progress bar
pb = ttk.Progressbar(root, orient='horizontal', mode='indeterminate', length=200)
pb['value'] = 0
pb.place(relx=0.3, rely=0.2)

# start button
start_button = ttk.Button(
    root,
    text='Start',
    command=lambda: start(pb)
)
start_button.place(relx=0.3, rely=0.3)

# stop button
stop_button = ttk.Button(
    root,
    text='Stop',
    command=lambda: stop(pb)
)
stop_button.place(relx=0.5, rely=0.3)

# CMD Box
# text = Text(root)
# text.pack()
# text.insert(END, 'TEST')

# Main loop
root.mainloop()
