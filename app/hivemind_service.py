"""
This file contain HivemindService and its implementation.

HivemindService provide functionality to train a machine learning (ML) model using decentralized method.
"""
from threading import Thread

import hivemind


class HivemindService(Thread):
    def __init__(self):
        super().__init__(daemon=True)
        self._dht = None
        self._opt = None

    def init_dht(self,
                 initial_peers=None,
                 use_ipfs=True,
                 start=True):
        """
        DHT is a decentralized key-value storage to be shared between peers
        :param use_ipfs: IPFS protocol; decentralized p2p connection
        :param initial_peers: ip address of the initial peers in an array of string
        :param start: start hivemind right away or wait for manual start command
        :return:
        """
        self._dht = hivemind.DHT(
            host_maddrs=["/ip4/0.0.0.0/tcp/0", "/ip4/0.0.0.0/udp/0/quic"],
            initial_peers=initial_peers or None,
            use_ipfs=use_ipfs,
            start=start
        )

    def init_opt(self,
                 run_id,
                 opt,
                 batch_size_per_step=32,
                 target_batch_size=10000,
                 use_local_updates=True,
                 matchmaking_time=10.0,
                 averaging_timeout=10.0,
                 verbose=True):
        """
        Decentralized optimizer for the current system.
        Use to adjust resource usage.
        :param opt: optimizer
        :param run_id:
        :param batch_size_per_step:
        :param target_batch_size:
        :param use_local_updates:
        :param matchmaking_time:
        :param averaging_timeout:
        :param verbose:
        :return:
        """
        self._opt = hivemind.Optimizer(
            dht=self._dht,
            run_id=run_id,
            batch_size_per_step=batch_size_per_step,
            target_batch_size=target_batch_size,
            optimizer=opt,
            use_local_updates=use_local_updates,
            matchmaking_time=matchmaking_time,
            averaging_timeout=averaging_timeout,
            verbose=verbose
        )

    @property
    def opt(self):
        return self._opt

    @property
    def dht(self):
        return self._dht

    def print_dht_address(self):
        print('\n'.join(str(addr) for addr in self._dht.get_visible_maddrs()))
        print("Global IP:", hivemind.utils.networking.choose_ip_address(self._dht.get_visible_maddrs()))

    def run_dht(self):
        self._dht.run()

    def exit(self):
        self._dht.shutdown()
