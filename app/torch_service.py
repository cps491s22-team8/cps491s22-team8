"""
This module contain the TorchService class. (WIP)

TorchService provide training functionality for machine learning (ML) model.
"""
from threading import Thread
import uuid
import hivemind
import torch
import torch.nn as nn
import torch.nn.functional as F
from torchvision import datasets, transforms
import wandb


class TorchService(Thread):
    def __init__(self):
        super().__init__(daemon=True)
        self._transform = transforms.Compose(
            [transforms.ToTensor(), transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])

        self._trainset = datasets.CIFAR10(root='./data', train=True, download=True, transform=self._transform)

        self._model = nn.Sequential(nn.Conv2d(3, 16, (5, 5)), nn.MaxPool2d(2, 2), nn.ReLU(),
                                    nn.Conv2d(16, 32, (5, 5)), nn.MaxPool2d(2, 2), nn.ReLU(),
                                    nn.Flatten(), nn.Linear(32 * 5 * 5, 10))
        self._opt = torch.optim.SGD(self._model.parameters(), lr=0.001, momentum=0.9)
        self._running = False

        wandb.login()
        wandb.init(name=uuid.uuid4()[:8],
                   project='cps491s22-team8',
                   notes='CIFAR10 Monitoring Loss',
                   tags=['CIFAR10', 'COOL', 'UD'],
                   entity='nmq12')
        wandb.watch(self._model)
        self._loss = 0
        wandb.config.lr = 0.001

    @property
    def opt(self):
        return self._opt

    @property
    def model(self):
        return self._model

    @property
    def running(self):
        return self._running

    @running.setter
    def running(self, r):
        self._running = r

    def train_data(self, opt: hivemind.Optimizer):
        while self.running:
            wandb.log({
                "Epoch": int(opt.local_epoch),
                "Valid Loss": self._loss,
            })
            for x_batch, y_batch in torch.utils.data.DataLoader(self._trainset, shuffle=True, batch_size=32):
                opt.zero_grad()
                self._loss = F.cross_entropy(self._model(x_batch), y_batch)
                self._loss.backward()
                opt.step()
                # print(f"loss = {loss.item():.3f}")
                if not self.running:
                    break

    def start_training(self):
        self.running = True

    def stop_training(self):
        self.running = False

    def exit(self):
        self.running = False
