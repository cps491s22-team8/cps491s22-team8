@echo off
docker run -u=%USERNAME% ^
	-e DISPLAY=192.168.10.12:0.0 ^
	-v /tmp/.X11-unix:/tmp/.X11-unix:rw ^
	-v %cd%/app:/app --rm dedloc