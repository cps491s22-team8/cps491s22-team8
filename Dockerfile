# Hivemind Docker image
FROM learningathome/hivemind

# Download Package Information
RUN apt-get update -y

# Install Tkinter + XServer
RUN apt-get install python3-tk x11-apps -y

# Install dependencies
# COPY requirements.txt /
# #RUN pip install -r requirements.txt

# Run application
CMD ["/app/main.py"]
ENTRYPOINT ["python3"]